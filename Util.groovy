import java.util.regex.Pattern

class Util {
    static Closure<Boolean> checkIfInteger = {String character ->
        character.isInteger()
    }

    static int provideCalculation(int num1, int num2, String operation) {
        switch (operation) {
            case "+": return num1 + num2
            case "-": return num1 - num2
            case "*": return num1 * num2
            case "/": return num1 / num2
            case "^": return num1**num2
            default: return 0
        }
    }

    static List<String> getListOfNumbersAndOperations(String value) {
        Pattern pattern = ~/[0-9]+|\W/
        Object matcher = value =~ pattern
        return matcher.findAll()
    }
}
