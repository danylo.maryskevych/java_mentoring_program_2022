import static Util.*

class Calculator{
    static def priorityMap = ["+": 1, "-": 1, "*": 2, "/": 2, "^": 3]
    static final String OPEN_BRACKETS = "("
    static final String CLOSED_BRACKETS = ")"

    static void main(String[] args) {
        String example = "1+2*(3+4/2-(1+2))^3+1"
        calculate(example)
    }

    static void calculate(String input) {
        println("Calculation of: ${input}")
        ArrayList numbers = []
        ArrayList operations = []
        List listOfNumbersAndOperations = getListOfNumbersAndOperations(input)

        for (elem in listOfNumbersAndOperations) {
            if (checkIfInteger(elem)) {
                numbers.push(elem)
            } else {
                if (elem == OPEN_BRACKETS || (operations.size() != 0 && operations.first() == OPEN_BRACKETS)) {
                    operations.push(elem)
                    continue
                }
                while (operations.size() != 0 && priorityMap[elem] <= priorityMap[operations.first()]) {
                    if (operations.first() == OPEN_BRACKETS && elem == CLOSED_BRACKETS) {
                        operations.pop()
                        break
                    }
                    String lastOperation = operations.pop()
                    int num2 = numbers.pop() as int
                    int num1 = numbers.pop() as int
                    numbers.push(provideCalculation(num1, num2, lastOperation))
                }
                if (elem != CLOSED_BRACKETS) operations.push(elem)
            }
        }

        while (operations.size() != 0) {
            String lastOperation = operations.pop()
            int num2 = numbers.pop() as int
            int num1 = numbers.pop() as int
            numbers.push(provideCalculation(num1, num2, lastOperation))
        }

        println("Answer: " + numbers[0])
    }
}